## Others

*   Wang PangZi "Fatty"
    *   Yun Cai
    *   Yun Agui (Yun Cai's father).
*   Liang Wan (phoenix tattoo), girlfriend of Zhang RiShan, originally Wang.
*   Hei YanJing "Black glasses" (immortal)
*   Qiao (Hendry Cox)
    *   A Ning
*   Da Kui
*   Ya Nv

## [The 10 families](https://merebear474765851.wordpress.com/2020/10/10/introduction-about-the-mystic-nine/)

### [Chapter 82](https://merebear474765851.wordpress.com/2020/04/14/chapter-82-postscript/)

> At that time, there were no rankings in the circle but the most famous ones were nine people — Chen Pi Ah Si, Old Dog Wu, Black Back the Sixth, and so on. The last one was Xie Jiuye, Xie Lianhuan’s father. There were also the so-called “Master Ten” and “Master Eleven” behind them, but the scope of recognition was very small.

### [The Mystic Nine was divided into three parts.](https://merebear474765851.wordpress.com/2020/09/20/wu-xies-private-notes-chapter-2-the-mystic-nine-leaders/)

> The upper three clans were all well-established old families, and their official status had been largely honest. They had legitimate businesses on the surface, and were powerful in officialdom, so they mainly relied on their own underlings when they went grave robbing. There were some extremely skilled and loyal underlings in the upper three clans, but because of this, their gradual aging resulted in the upper clans’ gradual decline.

> The middle three clans were the main grave robbing forces. They were all courageous heroes who had a few apprentices, and usually worked in the mountains all day. These people were relatively young, but they were very greedy and did everything, from murdering to stealing goods. Their reputations were made from killing people, so they had nothing to be afraid of. The big cases after the founding of the country all had something to do with them.

> The lower three clans were businessmen who specialized in antique dealing. Even though they had skills, they didn’t go grave robbing often. The lower three clans worked closely with the middle three clans. Since the upper three clans were on such a large scale, they had no profits left over for the lower three clans. As a result, the upper three clans had almost no interactions with the lower three clans."

### Wang

*   Want to make the secret public and to do so they must
    destroy the Zhang family
*   Wang XiaoYuan (female, spy of Wu Xie, crush of Li Cu, alias Shen Qiong)
*   Wang XianSheng (Wang cklan teacher)
*   Wang Can
*   Wang Cen
*   Su Nan

### Zhang

*   Mystic nine first ranked.
*   Their symbol is the Qilin.
*   Want to keep the secret hidden and have trained themselves
    to fight whatever is beyond the bronze door.

*   Zhang Fo Ye.
*   Zhang Rishan (lieutenant to Zhang Fo De).
*   Zhang Qiling (mother Ba Mai).
*   Zhang Haixe (looks like Wu Xie).
*   Zhang Haixing (adopted, XiongQui tattoo).
*   Zhang Haiqui (woman, leader of YueShang 13).
*   Zhang Chuntao 

### Hong

*   Mystic nine second ranked.
*   Thei symbol were red daffodils, became rhododendrons.
*   Hong ErYue.

### Li

*   Mystic nine third ranked.
*   Li BanJe
*   Li Sidi

### Chen

*   Mystic nine fourth ranked.
*   Chen Pi Ah Si.
*   Chen WenJin
*   Chen JinShui (former head of Chen clan)
*   Chen DingJu

### Wu

*   Mystic nine fifth ranked.
*   WuShanJu (home of the Wus in HangZou)
*   Wu Laogu (wife: Xie ???, was engaged to Huo XianGu)
    *   Wu YiQiong (first brother)
	*   Wu Xie ("no evil").
    *   Wu ErBai (second brother).
    *   Wu SanXin (third brother).

### ???

*   Mystic nine sixth ranked.
*   "Black Back The Sixth" ("a swordsman in Shaanxi before").

### Huo

*   Mysic nine seventh ranked.
*   Used to be first ranked.
*   Jin Shangzhu is their family company.
*   Huo XianGu (fairy) (husband: ???)
    *   "son who joined the revolution led by Mao Zedong".
    *   Huo Ling
    *   Huo ???
        *   Huo Xiu Xiu
    *   Huo Daofu
    *   Huo Xu
    *   Huo Youxue
    *   Huos SanNiang (trains Huo XiuXiu)

### Qi

*   Mystic nine eighth ranked
*   Qi TieZui (Bay Ye)
*   Qi Qiu
*   Qi Cui
*   Qi Yu (deformed body)

### Xie

*   *   Xie JiuYue
    *   Xie LianHuan (Xiao XieJiu)
    *   Xie YuChen
        *   Xie Hua (adopted by Xie LianHuan)

### Master 10

### Master 11

## Xisha tomb participants

1.  Zhang QiLing
2.  n.a.
3.  Li Sidi
4.  Chen WenJin
5.  Wu Sanxing
6.  n.a.
7.  Huo Ling
8.  Qi Yu
9.  Xie LianHuan

## Quotes

### [Chapter 36](https://merebear474765851.wordpress.com/2020/11/01/chapter-36-splitting-the-big-coffin/)

> This huge meteorite split into three pieces as it fell: one in Changbai Mountain, one in the Queen of the West’s country, and one near Changsha. Special ancient buildings were constructed around all of them. This meteorite has a great power, which lets people enter it to see the world of the dead.

### [Chapter 29](https://merebear474765851.wordpress.com/2020/05/08/chapter-29-the-zhang-family-falling-apart/)

> Did the Zhang family know something, so they used all of their accumulated resources to guard the giant bronze door?

### [Chapter 29](https://merebear474765851.wordpress.com/2020/05/08/chapter-29-the-zhang-family-falling-apart/)

> So, what was behind the bronze door? Let’s assume that there were three forces in the world at that time: one was the Eastern Xia people who used the giant bronze door; the other was the Zhang family’s people who occupied Eastern Xia with their families; and the other was Wang Zanghai, who discovered the existence of the Zhang family. Wang Zanghai must have been very curious about the complicated relationship between the giant bronze door, the Eastern Xia civilization, and the Zhang family. This secret must have something to do with the world behind the bronze door, and it was buried somewhere in the Zhang family’s ancient building.

> Wang Zanghai did a lot of things to reach his goals but soon found himself unable to cross the Zhang family’s huge net. Any news he spread soon disappeared. “So, the Wang family must destroy the Zhang family to achieve its goal?” I asked. “This can’t be done by strategy alone,” Zhang Haike said. “So the focus of the struggle between the Wang and Zhang families is whether to disclose the Zhang family’s hidden secrets, and the premise is that the Zhang family must be destroyed. Have I understood it right?” I thought about it some more, “That is to say, your aim now is to protect the secret, because your struggle has now reached the final stage.”

### [Chapter 50](https://merebear474765851.wordpress.com/2020/05/14/chapter-50-mud-pit/)

> “Do you know who this person is?” They looked at it and shook their heads. Zhang Haike glanced at the hole they had just come through and said, “This man is that kid’s father. I met him when I was a child.” Lifting up one of the corpses, Zhang Haike brought the torch close to its sinister face and said, “Look carefully at the faces and hands of these corpses. All of them are from the Zhang family, and all have the Zhang family’s characteristics.” “Eh?” The others went to look at the corpse’s fingers and turned pale when they found that the fingers were indeed a strange length. “What’s this all about?” “I knew that the kid’s father was dead, but the family was very secretive about his death. I’m afraid we were deceived this time. Someone probably arranged for us to come here.” “Who?” Zhang Haike looked back, “It’s that kid. We were led here the entire way. And if you recall, he provided almost all the information.” He paused and then continued, “This boy… was it for his father’s body that he lured us all here?” “Motherfucker, I’ll go up and wring his neck,” one of them said furiously, but Zhang Haike immediately waved his hand,

> “Wait a minute. These Zhang family members’ deaths are too suspicious. The kid shouldn’t want to harm us but wants us to see that these people died. He may just want to find out how his father died.” “No,” another boy said. “Come here and take a look.” They all turned around and saw that the boy had jumped into the mud and picked up another corpse. He twisted its head hard and the head easily popped off in an instant. “The neck is broken and these other bodies have a lot of injuries,” he said. “There was a fight here, and the Zhang family’s killing methods were used. Some of these people were murdered, and it was the Zhang family who killed them. This is the aftermath of infighting.”

> This so-called boundary was simply where the Zhang family’s exploration had ended, which turned out to be an ancient ship sunk into the silt. Zhang Haike saw the mummified bodies of three children piled up in the corner of the ship’s hold. They were obviously orphans of the Zhang family who had died when too much blood was collected, as evidenced by the obvious wounds on their bodies. The children were only seven or eight years old, which had Zhang Haike feeling angry and powerless.

### [Chapter 50](https://merebear474765851.wordpress.com/2020/05/14/chapter-50-mud-pit/)

> The patriarch’s hexagonal bell was different from other bells in that it was very big—almost as big as a cow bell — and the sound it made was very slight. But if one heard the sound, they would become clear-headed their soul would settle down. To put it bluntly, it could offset the effect of other bronze bells. The patriarch back then would have definitely been wearing this bell to avoid disaster.

> But the most important thing was that there was a room in the Zhang family’s old house where only the patriarch could enter. Every time the old patriarch was replaced with his successor, the new patriarch would enter that room and come out with the old patriarch’s corpse. If the old patriarch didn’t correctly estimate his time of death, the two of them would have to stay in there for several years before the old one would die. [...] The room was later moved to the bottom of the Zhang family’s ancient building and placed in their ancestral grave. The various secrets were also classified into different levels and categories, the most important of which was called the Ultimate. The key point here was that the Zhang family’s patriarch died in Sizhou City. The previous Zhang family patriarch knew the secrets of the world while the later Zhang family patriarch only got a powerful family. But the secrets in this stone room were the duty and purpose of this huge family, so from that moment on, they lost their reason for existing. Thus, the crisis began to emerge.

### [Chapter 53](https://merebear474765851.wordpress.com/2020/05/15/chapter-53-accident-after-the-explosion/)

> Thinking back on it, it should be like this: the Ultimate should be the core of what the Zhang family discovered a long time ago, which can be said to be the largest secret in the world. Poker-face knew of the existence of this secret but later went to see it for himself. The secret behind the huge bronze door under the mountains and rivers. Who built the door? What kind of world was behind it?

### [Chapter 26](https://merebear474765851.wordpress.com/2020/10/10/wu-xies-private-notes-chapter-26-the-giant-bronze-door/)

> Based on my visual inspection, the bronze door is about thirty meters high. I have some basic chemistry knowledge, so if the thickness of the door is five meters, then these two giant bronze doors weigh more than a thousand tons. There is absolutely no force in the world that can push them open. What’s behind them? What secrets are hidden at the bottom of Changbai Mountain behind these doors? There isn’t a trace to be found on the extremely cumbersome patterns. In the Eastern Xia Kingdom at that time, they probably couldn’t understand what happened in the birthplace of King Wannu. I really want to go in and have a look!

### [Chapter 182](https://merebear474765851.wordpress.com/2021/03/23/chapter-182/)

> Fatty also had the same idea and looked at me before saying, “You’re right. This thing was formed in outer space. It’s a metal meteorite.” “It’s like a bubble. The hollow meteorite crashed into the mountain long ago and solidified here. After being discovered, someone broke through the meteorite’s shell and polished and carved these things inside,” I said. So, these huge bronze installations embedded deep in the earth’s crust were all fragments of that huge meteorite. I was reminded of the giant bronze door in Changbai Mountain’s underground strata, the meteorite under the Queen of the West’s ancient city, the ancient bronze tree under Qinling Mountain, and the bronze cave here under the Himalayas. I touched the metal underneath and wondered, how could there be bronze meteorites? Is this really bronze? According to all the signs we found along the way, this meteorite passed over more than half of China or even a ninth of the earth. It breached the atmosphere in East China, disintegrated, broke into several pieces, and then crashed deep into the Earth’s crust.


### [Chapter 182](https://merebear474765851.wordpress.com/2021/03/23/chapter-182/)

> When we looked down, we saw a relief sculpture on the side that showed someone carving on the exposed part of the meteorite. Numerous people were chiseling the rock in front of the meteorite to expose the bronze and carving delicate patterns on it. Upon seeing this, Fatty suddenly leaned close to me and said, “Look, who are these people? Why do they… why do they have so many hands, like spiders?” “King Wannu,” I said while rubbing my eyes hard.

### [Wu Xie's private notes chapter 30](https://merebear474765851.wordpress.com/2020/10/14/wu-xies-private-notes-chapter-30-zombies-and-monsters/)

> Maybe there’s an ancient civilization that we haven’t discovered yet. Maybe people from that time domesticated and trained this kind of owl with a human’s face and bird’s body, and the owls played very important roles in wars and transportation. Moreover, this kind of owl might have appeared frequently in tribal wars, and may have transported a large amount of intelligence and medicine, making other tribes mistakenly think that it was a kind of god. This mysterious ancient civilization was obviously very low-key, or its mystical character led to all the remaining materials about it in the world being destroyed. The few remaining traces of these owls were at the giant bronze door under Changbai Mountain. They went extinct after the civilization had vanished, leaving only a few survivors underground to guard the last traces of their masters. It sounded far-fetched, but I figured it made sense. Furthermore, using owls to spread information had me thinking of the image of witches in Europe during the Middle Ages. Every witch there had an owl as a pet. This was a bit fun. Was it the magical world of Hogwarts behind the huge bronze door?

### [Wu Xie's private notes chapter 30](https://merebear474765851.wordpress.com/2020/10/14/wu-xies-private-notes-chapter-30-zombies-and-monsters/)

> Could it be some kind of evolutionary result or man-made choice? For example, in King Wannu’s clan, maybe one’s status was determined by the number of arms they had. People with only two arms were considered relatively inferior, and as the number of arms increased—and the degree of deformity became more serious— the status of those people would become higher and higher. Therefore, people in the clan hoped that their children could marry people with more arms. Those people who had more arms had higher status, so that meant they had more wives and concubines.

### [Wu Xie's private notes chapter 34](https://merebear474765851.wordpress.com/2020/10/19/wu-xies-private-notes-chapter-34-other-related-information/)

> Mural with captured Wang ZangHai [...] Note: Wang Zanghai lived in the late Ming Dynasty. This obviously meant that the regime didn’t disappear overnight, but gradually reduced from a country to a city-state, and from a city-state to a tribe.

### [Chapter 145](https://merebear474765851.wordpress.com/2020/08/06/chapter-145-the-truth-of-the-world/)

> “What King Mu saw in the Queen of the West’s country was the truth of this world. You’re right, the history I told you proved unimportant in the end. Too many kings and grave robbers have been attracted by King Mu’s story. They were led to believe that they were looking for a way to live forever, while King Mu actually brought them the truth of the world.” Not everyone could accept this truth. Some people died, some went crazy, and some calmed down and tried to understand it.

> The sound of applause came from outside the window. Li Cu didn’t know when he had appeared, but the former leader of the black-clad people clapped his hands and entered the ward. “He’s a little quick-witted.” The middle-aged man nodded to the black-clad person.

> Li Cu continued to ask, “But what’s the truth of the world?” “We don’t know.” The black-clad person said: “The biggest secret in the world has been hidden by Zhang Qiling’s family in a place we can’t enter. They must be the last people in the world who have come into contact with this secret. I don’t know why, but all those who come into contact with it choose to hide it forever.”

### [Chapter 146](https://merebear474765851.wordpress.com/2020/08/07/chapter-146-wang-zanghai/)

> A few people, however, discovered the secret behind immortality. [...] The latter group was characterized by the fact that the first half of their lives was very similar to the former, but their behavior suddenly changed dramatically at some point, becoming mysterious and uncertain. Unlike the clear path laid out in the first half of their life, this kind of person practically seemed invisible in the second half of their life, as if they didn’t leave any written records. The ancestor of the black-clad people’s organization was just such a person. The organization thought that this ancestor was a feng shui master called Wang Zanghai. The Wang family had a kind of inexplicable hatred towards Zhang Qiling’s family that came from their ancestors. The present generation had no idea what the origin of the hatred was, but they knew that they and the remnants of the Zhang family had been habitually hunting each other.

### [Chapter 146](https://merebear474765851.wordpress.com/2020/08/07/chapter-146-wang-zanghai/)

> Even though the Zhang family had hidden the world’s secrets, Wang Zanghai obviously knew what they were, and still fought the Zhang family anyways. One could almost theorize that the truth of this world had two meanings. The first was what Wang Zanghai clearly already knew: what is the truth of the world? The second one was: how do I use the truth of the world? This truth seemed like it wasn’t a concept, but something that was able to interact with the world; something that was changing and could be used. Zhang Qiling’s family chose to hide this truth, and it seemed like Wang Zanghai was trying to do something about it. To a large extent, this meant his behavior didn’t stem from a need to know the truth, but a need to get the truth.

### [Chapter 6](https://merebear474765851.wordpress.com/2020/12/28/chapter-6-those-who-listen-to-thunder/)

> I even thought of the rain and thunder I had heard in that dark video tape so long ago, which was said to have come from behind the bronze door. 

### [Chapter 82](https://merebear474765851.wordpress.com/2020/04/14/chapter-82-postscript/)

> The eighth story was the Snake Marsh Ghost City. There are two stories spliced together by clues that run throughout the whole Snake Marsh Ghost City story. [...] The first is the legend of Wang Zanghai. [...] The second is Tie Mian Sheng’s story, which was slowly beginning to take shape. Now you can clearly see the origin of the story — the huge bronze miracle in the mountains and the secret behind the Snake Marsh Ghost City. Throughout history, there were two people who transcended the times and got a glimpse of this secret: one was Tie Mian Sheng in the Warring States period, and the other was Wang Zanghai in the early Ming Dynasty.

### [Chapter 82](https://merebear474765851.wordpress.com/2020/04/14/chapter-82-postscript/)

> The third story takes place off the coast of Xisha. This was also the story of Wu Xie’s Uncle Three’s adventure at sea. The appearance of Zhang Qiling forms the biggest mystery in this story. There are two versions of the story: one was the deceptive version given by Uncle Three, and the other was the candid version Uncle Three gave after experiencing that catastrophe. The final truth was that both versions were lies because for Uncle Three, there was a huge secret that was related to Wu Xie.

### [Chapter 13](https://merebear474765851.wordpress.com/2020/05/01/chapter-13-notes-pertaining-to-the-worlds-ultimate/)

> It turned out that on a certain page of the notebook, something had been drawn. On the border, the foreigner wrote a note in crooked Tibetan. Laba didn’t know many words, but he understood this Tibetan sentence because a lama once said these things when he was worshiping Buddha. This sentence in Tibetan meant: “the world’s limit”. [...] There were many sketches interspersed within the notes and on the first few pages of that picture marked with the Tibetan words “the world’s limit”, I saw a huge bronze door. The bronze door was drawn in very delicate strokes, which made me think that the owner of this notebook was a master artist. I could see that the giant door wasn’t exactly the same as the one in Changbai Mountain, but I understood that it must be the same kind of thing.

### [Chapter 89](https://merebear474765851.wordpress.com/2020/05/26/chapter-89-religion/)

> “Feng isn’t my name, but forget it. It’s like this: in between Hongshan and Juyan — that is, the whole area from the present Heishui City to Hongshan — there was a huge civilization that can’t be dated. Because all of the archaeological clues were very strange, this civilization wasn’t recognized in your country and had a certain confidential nature,” Short Feng said. “There are three factors that contributed to this kind of secrecy. First, there were traces of this civilization appearing throughout different time periods. In other words, this civilization seemed to exist until the Yuan Dynasty, and all the signs seemed to show that it didn’t change much. But all the neighboring civilizations that lived in the same time period as this civilization had no records of it. It was spread out across the vast desert of western Liaoning but it seemed like it hadn’t been seen or even touched by anyone. “Second, all the information about this civilization seemed to have been taken back to Japan when they invaded China. There isn’t much evidence of the civilization itself because Torii didn’t disclose too much about it. It’s said that the systematic features of this civilization make it difficult to find again. “As a result, without the basic information, no one in China is willing to carry out a second archeological expedition. It’s tantamount to a wild goose chase. “Third, many basic features of this civilization are subversive.” He touched his chin. “This civilization’s religion was very similar to Buddhism but the details are completely different. If we assume that Indian Buddhism originated from a ‘key factor’, then this civilization also had very similar ‘key factors’, but the resulting religion was completely different.”

### [Chapter 82](https://merebear474765851.wordpress.com/2020/04/14/chapter-82-postscript/)

> At that time, there were no rankings in the circle but the most famous ones were nine people — Chen Pi Ah Si, Old Dog Wu, Black Back the Sixth, and so on. The last one was Xie Jiuye, Xie Lianhuan’s father. There were also the so-called “Master Ten” and “Master Eleven” behind them, but the scope of recognition was very small. They were all blocked by themselves or their subordinates, and when it came to outsiders, no one knew. Some people said that Chen Pi Ah Si was now more than ninety years old, and he was more than forty years old fifty years ago. At that time, Old Dog Wu wasn’t very old. If he was seventeen at that time and it took him ten years to become famous, then he would’ve been twenty-seven by then. How could he be ranked behind Chen Pi Ah Si, who was nearly fifty years old, and become Old Dog Wu? If this line went on like this, wouldn’t little Xie Jiu still be wearing open-crotch pants? It was a bit unreasonable, but those who had some common sense would know that the rankings in the circle weren’t based on age, but on skills and seniority, which were all ranked by others. Old Dog Wu was high in rank, which showed how powerful his wrists and drive were at that time. They made people incapable of refusing to obey him.

### [Chapter 82](https://merebear474765851.wordpress.com/2020/04/14/chapter-82-postscript/)

> The earliest thing that happened was at the Changsha Dart Summit. In the early days of the founding of the People’s Republic of China, several grave robbers stole the most important object in this book — the silk book of the Warring States Period — from an ancient tomb. This was the story of Wu Xie’s grandpa’s generation, of Old Dog Wu when he was young. [...] The second story also happened at Dart Summit. That was the incident in which Wu Xie’s Uncle Three encountered the blood corpse in the tomb he entered when thwarting the American, Qiu Dekao, twenty to thirty years after the first story. It could be argued that this incident was entirely coincidental, but Wu Xie’s Uncle Three learned what had happened when Wu Xie’s grandpa and his family encountered the blood corpse before. This time around, Uncle Three gained some experience and got a strange pill. Although this was only an episode, it can be argued that this incident was the cause of the Xisha incident.

### [Chapter 76](https://merebear474765851.wordpress.com/2020/05/24/chapter-76-dealing-with-that-group-of-things/)

> So, they sent for someone to open the coffins. The first few were fine so everyone had been slacking off until they got to the sixteenth coffin. As a result, the coffin was already half-opened when they found that it was covered with a layer of bronze and there was nothing inside of it. They thought that it was an empty coffin, but when they went to climb in and search carefully, the contents suddenly came out. The situation was exactly the same as what we had experienced earlier — the contents couldn’t be seen against the bronze casing but suddenly appeared in the air. [...] Then we saw the thing’s hands begin to move as those spider-like arms behind it slowly assumed the appearance of some Buddhas portrayed in religion. “Thousand-Armed Guanyin Corpse.” It suddenly dawned on me that this was a thousand-armed Guanyin corpse in armor. This was another King Wannu. Bronze door, canyon, thousand-armed Guanyin…it was all such a familiar combination. [...] After the armor was completely blown off, King Yama’s upper body was completely exposed. Its rock head was basically a huge stone ball like a human head that had been inserted into its trunk, which contained a dried-up corpse covered with short black hairs. A large amount of sand flowed out of the hole that had been blasted in it. “Fuck, someone shaved a zombie,” Fatty said. “This is a fighting corpse.”

### [Chapter 67](https://merebear474765851.wordpress.com/2020/05/20/chapter-67-biggest-secret/)

> “There’s no time,” Fatty said to Zhang Haixing. “That’s what your patriarch said at the time.”

### [Chapter 72](https://merebear474765851.wordpress.com/2020/05/23/chapter-72-extremely-chaotic-situation/)

> Based on the blood’s movements, I knew that this thing was climbing to the ceiling. It was a behemoth, and it had been lying on the cave wall. In a flash, I was almost able to make out what this thing looked like. It was a huge transparent multi-handed monster, just like a giant fleshy spider. At the same time, Zhang Haixing finally fired. She had obviously reached the same conclusion as me and showered the cave wall with bullets. I didn’t see any hit the creature, but they hit the cave wall without any obstruction, causing sparks and blood to spatter out, along with the endless sound of impacting metal.

### [Chapter 187](https://merebear474765851.wordpress.com/2020/08/23/chapter-187-information-fragments/)

> I coughed violently, knowing that pneumoconiosis (2) would occur sooner or later. But I was laughing when I coughed.

### [Chapter 195](https://merebear474765851.wordpress.com/2020/08/28/chapter-195-longzhi/)

> Che Zong said, “This kind of thing is called a Longzhi. It’s similar to a fox, and is often mistaken for them. It’s very rare now. This thing usually consists of nine things acting together: one female, which is very big, and eight males that are relatively small. The males often lay together on the female’s back, and because their bodies are very slender, the Longzhi is generally said to have nine heads and nine tails. In terms of identification, the legend of the nine-tailed fox came about because many people misunderstood the Longzhi.” “Bullshit.” I said: “I can tell just by hearing the name that it’s something from an ancient book. You say that’s what it is, but what basis do you have? Did your family used to raise them? Or have you eaten one?” Che Zong said, “You’re the young master of the Wu family and you don’t even know this? Not only do you not know, but you still don’t believe it? Your grandfather was the first person to discover the Longzhi. Sixteen dogs died when he caught the first one.”

### [Chapter 195](https://merebear474765851.wordpress.com/2020/08/28/chapter-195-longzhi/)

> “It’s because the Longzhi wasn’t a wild animal either. It was another person’s dog.” Che Zong answered. “I think your grandfather saw this and started specially training his dogs to deal with those people. You may not know this, but your grandfather raised a group of special military dogs and police dogs in northwest China. Luo Ruiqing personally asked him to breed dogs. At that time, your grandfather’s condition was that all the dogs had to be raised in a village by the youngest son of each family. After they were raised, the dogs would join the army together with the youngest son, and become military dogs. Because the youngest son’s future was integrated with these dogs, they received the best education and care, and also cultivated extraordinary feelings. These children and dogs later entered the central government to do security work.”

### [Chapter 199](https://merebear474765851.wordpress.com/2020/08/29/chapter-199-strategy/)

> The Longzhi king often attacks only once. Although Brother Xiao Man is a strong deterrent, the Longzhi king has more mobility than dogs. He can lurk in the soil, climb trees, and glide in the air briefly. Brother Xiao Man can defend us here in such an open space, but he can’t save our lives.” Brother Xiao Man must be the name of this big German Shepherd. When Che Zong called it by its name, Brother Xiao Man looked up at him and yawned. I just kept wondering why he looked so stupid when he wasn’t showing his teeth. Didn’t German Shepherds have wolf faces? This dog looked like Fatty when he was drunk.

### [Chapter 2-4](https://merebear474765851.wordpress.com/2020/09/04/chapter-2-4-fantasy-extra/)

> Smoker said softly, “Phoenix, you, Big Brother Zhang, and Snake Ancestor go into the water and check. This village is all wrong. We can’t trust that boy. You all go and ambush first. We’re too passive on the boat. I didn’t know who Big Brother Zhang and Phoenix were, but I saw the woman tut. She clearly didn’t like the idea. On one side, Poker-Face had opened his luggage, and took out a small thing similar to a life jacket. I wondered if it was the legendary shuikao (1). (1) One-piece diving suit made by the ancients with fish skin, sea scorpion skin, or shark skin. [...] I also moved and opened my rattan suitcase, finding that there was a black and blue snake inside that was as thick as my arm. I slowly put the snake down into the water, took off my coat, put the “shuikao” on, and sank into the water. Poker-Face slipped into the water at almost the same time. The serpent twisted its body in the water and then wrapped around me. It appeared Snake Ancestor was my nickname.

### [Chapter 2-9](https://merebear474765851.wordpress.com/2020/09/05/chapter-2-9-fantasy-extra/)

> At that moment, a strange and colorful bug crawled out of the bird down. It looked like a centipede, but all of its front legs were very long, and when it spread them out from both sides of its body, they looked like a hand or crown. Its lower body was similar to a centipede, but thinner, and every part of it was a different color. Moreover, these gorgeous colors made it seem like the photoshop saturation had blown up. There were so many ostentatious insects in nature, but I was surprised to see this one raise its upper body and spread all of its long legs open, just like a proud peacock displaying its feathers. It was as big as a cattail fan. At that moment, I suddenly felt weak all over, and I realized that this was a creature that didn’t belong in this world. This bug must have come straight from hell. Even though it was as an insect, it was too confident. Its contempt for other creatures meant it no longer fell under the category of “bug” and was instead more like an advanced creature.

### [Chapter 2-17](https://merebear474765851.wordpress.com/2020/09/09/chapter-2-17-fantasy-extra/)

> If Pampered Guy didn’t have any superior skills, then these snakes would be really hard to defend against. Just as I was starting to get anxious, I saw him reveal the iron piece under his tongue, and then spit out a small hexagonal bell earring that had also been under his tongue. As he put it on and smiled, a slight breeze caused the faint sound of the bell to spread through the air.
> [...] Pampered Guy was still waiting for me underwater and hadn’t left me behind. He watched me come down, made a “come on” gesture, and then carried the lantern and swam forward. This guy was more organized, disciplined, and responsible than Poker-Face, and I said to myself, damn, Poker-Face really is scum when it comes to organizational discipline.
> [...] Pampered Guy took off his shirt and revealed his thin upper body. He had a strange tattoo that was similar to a Qilin but wasn’t. Fortunately, this Little Master Three was experienced and knowledgeable, and I knew at a glance that it was a Qiong Qi.
> [...] I followed suit and heard Poker-Face say: “After finding the nest, I’ll continue to give chase.” “Patriarch, I’m the kind of person who does the dirty work, so I should be the one doing it.” Pampered Guy said. “You have to make it to the right time.” [...] He pointed to his tattoo: “You saw that my tattoo looks like this. Although my boss and I are surnamed Zhang, we’re not related by blood. I was adopted.” [...] But up until now, all his dirty work only made Little Brother’s facade of coolness stronger and stronger. The Zhangs were an outlandish family and deserved to be extinct.

### [Chapter 2-22](https://merebear474765851.wordpress.com/2020/09/11/chapter-2-22-fantasy-extra/)

> “This statue came from the woods, and the Americans went in to look for it? What kind of place is it?” “No one knows, it’s a huge mystery. And no one knows where this statue came from, either. That Bimo from eighty years ago was the only one that left a few clues.” Pampered Guy walked around the divine iron: “You might not be able to tell, but this thing is metal, very light, and of a very advanced craft. Ah, you really don’t understand. It’s rare to use bronze to make such thin things. The Americans have always suspected that there’s something inside, but the Bimo wouldn’t let them touch it.”

### [Chapter 2-26](https://merebear474765851.wordpress.com/2020/09/12/chapter-2-26-fantasy-extra/)

> He didn’t know what kind of bug it was, but the safest way was to burn and kill it. Poker-Face stopped him. [...] I didn’t know why this bug made him feel this way until I saw his injured hand. His palm had been cut, and the footprints on the floor led close to the female bug’s corpse. He had tested the green beetle, but it obviously had no reaction to his blood. “This bug is from that door.” Poker-Face said faintly: “Be careful, it will attract those things.”

### [Chapter 2-28](https://merebear474765851.wordpress.com/2020/09/13/chapter-2-28-fantasy-extra/)

> Pampered Guy handed me half of his drink. “Drink it drink it, before it’s all gone. This medicine is a strong tonic, but don’t worry, it won’t conflict with your snake medicine.” I sniffed it carefully, reluctantly took a few sips, and realized that there was something like human blood in it. After drinking, both Zhang brothers began wiping their hands and necks with the tea.

> Wash the tea off your body quickly. As time goes by, it becomes more poisonous than those bugs.” “So dangerous?” “How else did you think you could live up to this point?” “Then you should have told me in advance.” After washing their bodies, the three men washed their clothes, went back to the place where they had tea before, and started drying their clothes again. Snake Ancestor asked what was going on. Pampered Guy poured the tea water into the lake, threw the tea set down, and then found another tea set to make fresh tea that didn’t smell of blood. “That tea just now is a kind of poisonous corpse liquid, and the toxicity comes from some kind of red bug, which is very strong. But if you mix something with it, you can prevent the corpse poison from taking effect for a certain period of time, and at the same time restrain other bugs’ poison. If you apply it to your body, it can save you a few times as long as you’re not badly hurt. We’re clearing the way for what comes next.” [...] He then said to Snake Ancestor: “From now on, your surname is Zhang and your name is Zhang Xiaoshe.”

### [Chapter 2-28](https://merebear474765851.wordpress.com/2020/09/13/chapter-2-28-fantasy-extra/)

> “What do you think would have happened if the army had come in instead of us? How many people could survive?” Pampered Guy touched his stomach and seemed a little hungry: “We were so conscientious while escorting these people along the way, yet they’re still on guard against us.” “You mean, you came in early to protect those in the underworld?” Snake Ancestor was surprised and asked, “Why?” “We’ve always been like this.” Pampered Guy said. “It’s because all of you are too confident in yourselves. Many people simply don’t know how many things we’ve done for them. They already feel that they’ve traveled through a mountain of knives and a sea of fire. It seems scary how lucky they are. What they don’t know is that we’ve already cleaned up the road in front of them, just like what we did in this village.”

### [Chapter 3-15](https://merebear474765851.wordpress.com/2020/09/20/chapter-3-15-trap-extra/)

> A long time ago, I heard many legends of heavenly taught epics in Nepal and Medog. After a serious illness, many people could suddenly recite the epics that had millions of words. They didn’t say that it was handed down from husband to son or from master to disciple, but that these words appeared in their minds overnight. [...] Such legends not only occurred in Tibet, but also in the Central Plains and northern China, and there were a lot of them. Known locally as “zhuangxie”, it occurred when a person suddenly acquired a lot of information and images in his head. Unable to deal with the huge influx of information in such a short amount of time, his behavior became abnormal. I didn’t know Lu Sha, but the fact that he was enclosed here in the dark for more than ten years — during which time he kept drawing such complicated design patterns — seemed similar to a certain person’s behavior. The Zhang family had very long lifespans. It was said that after reaching a certain age, some of them would also experience the heavenly taught phenomenon, and would be driven to do some strange things by the information suddenly appearing in their minds. These things often had various subtle influences on the world.

### [Chapoter 3-17](https://merebear474765851.wordpress.com/2020/09/20/chapter-3-17-search-extra/)

> First, I still needed to find what Uncle Three left here. That thing’s importance definitely wasn’t inferior to the value of the bronze door, but there were only three of us here, so we needed more trump cards to keep us safe. [...] “Well, please give me a detailed explanation of what this ring looks like.” I said. “There’s a ghost head on it.” She tried to recall it, but started gagging once she thought of it. Ghost head. I took a deep breath and an old image suddenly popped into my head. I still didn’t know whether this ring had practical functions, but based on the hints on the relief back in those days, it had a very strong connection with the Ghost Seal’s earliest totem. All the materials related to the Ghost Seal recorded that it had three grooves, which happened to be the heads of three ghosts. I had guessed at that time that it was probably related to the ring worn by the one holding the seal.

### [Chapter 12](https://merebear474765851.wordpress.com/2020/09/27/chapter-12-stand-up/)

> The shadows of the trees on the other side were getting closer and closer to Wang Meng, and those “big trees” were gathering at a speed that could even be seen by the naked eye. I had a flash of inspiration, took out the IPAD, and saw that all the GPS signal points were heading in Wang Meng’s direction, but the shape had changed into a circle. “Those aren’t trees. Those are giant centipedes that are standing up.” I said.

### [Chapters 22-24](https://merebear474765851.wordpress.com/2020/10/03/chapters-22-23-and-24/)

> The human-faced birds could only consume the feces of the monkeys in their mouths. I first saw this kind of interdependence in the waterway leading to the Seven Star Lu Palace, when I saw the Warring States Period bell on that corpse-eater. I didn’t know who built the Yin and Shang imperial tomb at the bottom of Changbai Mountain, but it was the same as that of the Seven Star Palace. It was obvious that this kind of technology was wide-spread at the time.

### [Chapters 22-24](https://merebear474765851.wordpress.com/2020/10/03/chapters-22-23-and-24/)

> “My grandfather ordered us to cremate him when he died.” I said softly and coldly: “Granny Huo’s skin and Chen Pi Ah Si’s lifespan were also somewhat strange. As long as they were active, these people in the Mystic Nine’s middle and lower clans weren’t very normal in their later years. I don’t know what they experienced when they conducted the largest grave robbing operation in history.” “What do you mean?” “I wonder what my grandfather would have become if he wasn’t cremated.” Chen Pi Ah Si wasn’t cremated, and his body must have been brought here. If he had the same constitution as my grandpa, then I might finally know why Grandpa had to be cremated.

### [Chapter 25](https://merebear474765851.wordpress.com/2020/10/03/chapter-25-zombie/)

> After knowing him for so many years, Fatty has never been speechless, so I grabbed the binoculars and looked at where the flashlight was pointing. I saw a naked old man standing upright in the darkness. His skin was purple and looked as dry as bark under the light, and I could see his hands hanging by his sides. His fingernails stretched down into the water. “Grandpa Si?” My hands began to shake. Although I had already guessed it, it was still shocking to actually see the dead body of an old acquaintance rigidly standing here after ten years. [...] I pulled my flashlight beneath the water, and then moved closer. In two steps, the light beam passed through the muddy water and showed the portion of the corpse that was underwater. From where it was standing in the water, I could see that its skin was wrinkled and pale, and it almost looked like a skinny wax figure soaked in formaldehyde. I saw a tattoo on his body, but it definitely wasn’t a Qilin. It looked like something from the old society. It was a very pale cyan, but I couldn’t see what it was because of the wrinkles. It was Grandpa Si. Even though I couldn’t see his face or eyes, I definitely recognized these tattoos.

### [Chapter 31](https://merebear474765851.wordpress.com/2020/10/04/chapter-31-key/)

> Chen Pi Ah Si was a very smart person. He probably didn’t want to know the truth, and only wanted to solve his physical problems. Maybe he got the original Warring States silk book and distributed it to those in the business until finally, Uncle Three solved its secret. Based on my later understanding of him, when Uncle Three saw the silk book, he must have used it to set up a big trap. But Chen Pi Ah Si didn’t know this, so when he heard that Uncle Three was going to look for the ancient tomb on the silk book, he lent Zhang Qiling to him. Poker-Face finished his work in the Seven Star Lu Palace, but I still didn’t know what he did after that. Based on what I knew now, he had taken the ghost seal, changed the silk books, killed Iron Mask who was in the jade armor, and then brought Chen Pi Ah Si the anti-decaying key. All of this seemed to have been prepared for Chen Pi Ah Si to come to the Heavenly Palace. But I could still remember those feelings of doubt. When Poker-Face was in the Seven Star Lu Palace, I felt as if he had been there several times before. But based on his character, if he didn’t want me to know, he would’ve flawlessly pretended that everything was fine. I think the reason I noticed was because he started to recover his memory in the process of entering the Seven Star Lu Palace. Even he didn’t expect it to happen. In the process of assisting Chen Pi Ah Si to enter the Heavenly Palace, his memory completely recovered, and he already knew his purpose. That was why he finally entered bronze door instead of Chen Pi Ah Si.

### [Chapter 31](https://merebear474765851.wordpress.com/2020/10/04/chapter-31-key/)

> Zhang Qiling said that he was one of those in the Nine Gates who undertook the task of entering the bronze door.

### [Chapter 31](https://merebear474765851.wordpress.com/2020/10/04/chapter-31-key/)

> Was that the cost of immortality? Many people in the Mystic Nine got the seeds of immortality during the biggest grave robbery in history, but the whole process had to be completed behind the bronze door. They needed to find the bronze door within a certain period of time, so that was why everyone went crazy looking for clues all over China in the 1970s. During this process, people were becoming zombies, and those who provided support started to die. In the end, only Chen Pi Ah Si remained.

> I took a deep breath and felt a chill in the darkness. I was the eldest son. When did my grandfather give birth to my father? Was it before or after the biggest grave robbery in history? Why was my grandfather so emotional after I was born and named me Wu Xie (1)? What was this evil? (1) Remember, Wu Xie’s name sounds like “no evil”

### [Chapter 31](https://merebear474765851.wordpress.com/2020/10/04/chapter-31-key/)

> Under the fluorescent light, I finally saw the tall soldiers in armor standing neatly around me. Their faces were so long that they didn’t look human. I knew them. Zhang Qiling had first entered the bronze door dressed in their armor. Their eyes were pure white like Chen Pi Ah Si’s had been, but their eyelids had been cut off and their bodies were covered in dust. I hadn’t been to this place before. It must be deep in the underground crevice, for when I looked up, all I saw was more darkness. I deviated from the direction the key gave me and walked through the first row of armored soldiers. This was where Little Brother had set off for the Heavenly Palace, so I walked around, hoping to see some clues.

### [Chapter 41](https://merebear474765851.wordpress.com/2020/10/08/chapter-41-the-finale/)

> In the dim light, I saw the bronze door open. I’m just like the little match girl (2), I thought as I rubbed my face and opened my eyes. Sure enough, nothing had actually happened. [...] My consciousness began to blur, but I didn’t stop talking. I don’t know how long I stayed up, but in my hazy state, I suddenly felt someone slowly sit down beside me. I hesitated for a moment, glanced sideways, and saw the other person looking at me as well. Fatty slowly woke up and looked at us. I saw a familiar face and indifferent eyes reflecting the light of the bonfire. People say that when you forget someone, the first thing you forget is their voice. But when he spoke, it wasn’t unfamiliar at all. “You’ve aged,” he said The music was still flowing in this place close to hell.

### [Chapter 45-3](https://merebear474765851.wordpress.com/2022/04/16/chapter-45-3/)

> Black Glasses soon fell asleep as well. He didn’t have time to think about the box anymore. He needed to get some rest right now because he knew what was going to happen after they landed. After living for so long, it was finally here.

### [Chapter 100](https://merebear474765851.wordpress.com/2024/05/23/chapter-100-unable-to-escape/)

> Er Ye and Ba Ye, they knew that the Mystic Nine couldn’t handle you and that you wouldn’t enter the bronze door, so they made this backup plan to ensure that you’d get to this point.” “Are you trying to fucking say I’m special?” “Of course not. You’re just an unlucky guy with an unlucky name. But since it’s you, you have to go in!” Wen Binghui said, suddenly struggling. He somehow managed to reverse our positions so that he was the one holding me down and pushing my face into the hole. The sounds around me suddenly became muffled, and I found that the inside of the hole was very smooth, like the inside of a shell. The end of the hole was pitch black and bottomless, but I knew that there was something inside. I immediately felt that this thing was a person, and they were watching me from the darkness deep within the hole. I even sensed that this person was very familiar… “The answer is inside,” Wen Binghui murmured, looking vaguely in my direction from where he was lying on the ground. “Don’t be the one waiting outside. When they all wake up, you won’t have a chance to go in.”

### [The Yueshang thirteen](https://merebear474765851.wordpress.com/2022/01/23/the-yueshang-thirteen/)

> So, when I saw the words “Yueshang” again, I finally became interested. This was because it was probably an organization related to the Zhang family. I had very few clues to go on. All I knew was that Yueshang was founded around 1920, its members were all women, and there were thirteen of them in total so they were called the Yueshang Thirteen. Zhang Haiqi was probably one of the leaders, which was the earliest clue I had. I also knew that not all of its members were from the Zhang family. The latest clue I found said that a woman with a phoenix tattoo appeared among the Yueshang Thirteen. I was completely shocked when I read about the description of her appearance because I thought I might know this woman. She was a long-lost friend.

> Yueshang’s plan from the very beginning was called “Rotating Moon”, but no one knows what the specific plan entailed. Whatever it was, I instinctively felt that this plan was very important. This was because I had detailed records of Little Brother Zhang’s experience but Zhang Haiqi didn’t reveal anything about “Rotating Moon” to him during that time. They experienced thrilling adventures in which they faced death countless times but Zhang Haiqi didn’t reveal anything at all. It’s difficult for me to understand why Zhang Haiqi didn’t disclose anything — did she think that it was unnecessary to talk about it, did she just not want to talk about it, or maybe she couldn’t talk about it? Then, one night, I suddenly had a flash of inspiration. aybe this plan had nothing to do with men. Maybe this was a top secret plan only related to women that had been going on for so many years. “Rotating Moon”…what was it? We all know the laws regarding the moon’s rotation — since its rotation and revolution speed are matched, that means that we can only see its front side forever. In other words, its back is always facing away from us. If we hadn’t been able to launch moon-orbiting satellites, we would have never been able to see it. But the so-called concepts of front and back are just wishful thinking on our part. It’s only natural that we regard the side that humans have seen for tens of thousands of years as the front while the opposite side is regarded as the back. “Rotating Moon”, which I feel carries the meaning that people will never be able to figure out what the secrets are, is an extremely confident plan with a strange amount of certainty that absolutely no one knows about.

### [Fei Kun Balu – Zhang Chuntao From The Zhang Family](https://merebear474765851.wordpress.com/2022/04/23/fei-kun-balu-zhang-chuntao-from-the-zhang-family/)

> Nowadays, the Fei Kun Balu temple in Hainan is still a popular place for worship. Moreover, two percent of the incense money is transferred to an old bank account which has existed since the end of the Qing Dynasty. Its latest activity occurred when someone made a new passbook in the 1970s. No one knows who is running this account, but after doing some rough calculations, the amount of money in it should be astronomical. The account holder’s name is Zhang Chuntao. Although the account has existed for a long time, no one has ever used the money in it. Many Fei Kun Balu temples have illustrations of Zhang Chuntao. He is portrayed as a little immortal on the side, a tiny fat man who is holding an abacus and a watermelon. Poker-face has no memory of this person so his existence is a mystery to me.

### [Chapter 7-14](https://merebear474765851.wordpress.com/2021/01/25/chapter-7-14/)

> He didn’t believe that he would just randomly meet a bride who would react upon seeing his tattoo. If he thought about it in reverse, the only way for this to make sense was that everyone in Potluck Capital recognized this kind of tattoo, which meant the patriarch had great influence here. As he finished this thought, he said to Wulang Huazha, “Hey.” He then leaned forward under Wulang Huazha’s armpit, pulled his clothes open, and lit a flare to illuminate his exposed chest. “Does everyone here recognize these kinds of tattoos?” Wulang Huazha glanced at Little Brother Zhang’s chest, his face instantly turning pale when he saw the tattoo. Little Brother Zhang immediately knew the answer. Wulang Huazha stopped his horse and knelt down. “I didn’t know you’re such a formidable figure. Since you’re so extraordinary, I should’ve known you work for Fei Kun Balu.” “Fei Kun Balu?” Little Zhang thought for a moment. “Balu” was a mythological title that meant “warrior”. Little Brother Zhang got off the horse and fixed his clothes. “Everyone here knows our Master Fei Kun?” “People in many of the surrounding villages here worship Fei Kun Balu. The believers all have tattoos like this on their chests. You can go to them if there’s an injustice and there’s a possibility that Fei Kun Balu himself will come out and help,” Wu Lang Huazha said as he lowered his head. “What?” Little Brother Zhang touched his chin. “Religious leader?” His eyes lit up. “Not only has the patriarch established a clan here, but he also has a religious community. No wonder he’s the patriarch.”

### [Extra 7 Chapter 7-13](https://merebear474765851.wordpress.com/2021/01/24/chapter-7-13/)

> He Jianxi took it and saw that it was a strange bone that was covered in strange, dark red bumps. The moment he took it, he realized that it was very heavy. [...] “Those hunters go hunting all the time and have captured many things, but they had never seen this kind of bone before. No animals have this kind of bone.” The middle-aged man continued, “Mawei Mountain is inland and there aren’t any lakes or rivers around it. There’s only spring water. They put the bone aside until one day, a foreigner went there to build a church on Mawei Mountain. He saw this bone and told us that it was a Long Human bone. He said that there was a Long Human in the mountain, and the wild boar must have eaten the Long Human’s body.” He Jianxi had never heard of “Long Human” before and figured that it was probably the phrase the missionary had come up with when he was trying to translate what he knew into Chinese. “Over the next few years, the hunters captured wild boars and wolves and found these kinds of bones in their stomachs one right after another. Each bone looked stranger than the last,” the middle-aged man said. “The hunters were very scared and took the bones they had collected and began connecting them together. They wanted to know what was living in the mountain. But the more they put all the bones together, the more frightened they became.” The middle-aged man spread the straw mat out on the table. He Jianxi saw that the straw mat had been wrapped around some broken bones, which had all been glued together with mud to form a strange shape. It was a spinal column, but the vertebrae joints were much longer than that of any animal he had ever seen. The middle-aged man put seven or eight broken vertebrae together to form a complete spine that was more than three meters long. He Jianxi took a few steps back. He initially thought that it was a giant snake, but the middle-aged man connected some other bones together and a leg appeared. This leg bone was longer than any creature He Jianxi had ever seen before. This was a humanoid creature with a very long body and extremely long hands and feet. It looked like a giant stick insect. “Is this a Long Human?” He Jianxi took a shuddering breath.

### [Extra 7 Chapter 7-12](https://merebear474765851.wordpress.com/2021/01/23/chapter-7-12/)

> One time, Zhang Haiqi took a bath with Little Brother Zhang. He couldn’t remember what year it was, but he did remember that he was already 1.7 meters tall by that point, which was taller than Zhang Haiqi. Zhang Haiqi walked in naked as if no one else was around, the curve of her waist like that of a crescent moon. She shook her head and let her long hair flow down her back. Her hair was draped over her snow-white shoulders, her petite body was very well-proportioned, and she was a young girl as pretty as an elf, but her gaze was extremely mature and enchanting. Zhang Haiqi’s body had been specially trained so that her well-developed muscles were hidden under her soft skin. Her bones were also small, so she looked petite despite her plump figure. When she was walking, the parts on her body that should shake would shake, and her movements wouldn’t give off the impression that they were rigid. That day, Little Brother Zhang felt that Zhang Haiqi’s body was a little dazzling. He hadn’t thought her body was special that morning, the day before, or even a week before that, but at this moment, he suddenly felt different. He began to breathe rapidly. "Is this what women look like?" For the first time in Little Brother’s Zhang’s life, the word “woman” had a special meaning in his mind.

> For a long time, Little Brother Zhang would react the same as other men whenever he saw a petite woman. He also remembered that there was one time when Zhang Haiqi was so devastated that she started crying. It was when she thought Little Brother Zhang had died. Little Brother Zhang dragged his wounded body out of the mountain he was training in and walked for three days before he returned to Zhang Haiqi’s side. That was the first time he ever saw Zhang Haiqi cry. Although she recovered and didn’t show her emotions the next day, it was her tears that day that supported Little Brother Zhang until now. Before that, no one had ever cried for him. That night, Zhang Haiqi held the injured Little Brother Zhang tightly without letting go. She slept soundly, but Little Brother Zhang leaned against her plump chest with his eyes wide open until dawn.

<!--
  ;;; Local Variables:
  ;;;   mode: markdown
  ;;;   eval: (auto-fill-mode 0)
  ;;;   eval: (filladapt-mode 0)
  ;;;   eval: (visual-line-mode 1)
  ;;;   eval: (whitespace-newline-mode 1)
  ;;;   truncate-lines:         nil
  ;;;   indent-tabs-mode:       nil
  ;;;   fill-column:            78
  ;;;   tab-width:              8
  ;;;   case-fold-search:       t
  ;;; End:
  vim:set noet wrap lbr ts=8 tw=78 sw=2:
-->
