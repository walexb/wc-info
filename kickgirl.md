# Introduction

These are non-official notes and links to the strips of
Kick-Girl, the wonderful fantasy+science-fiction web comic by
Val Hochberg

# People

*   _Arcadia_: deputy general of the Crusaders
*   _Delilah_: Kick calls her "Little Flower", "my responsibility"
*   _Ms J_: sister of Six, made a film about her
    [0259](http://www.kick-girl.com/?comic=page-259)
*   _Raz_: Recordkeeper for Arana and the Crusaders
    [0173](http://www.kick-girl.com/?comic=page-173)
*   _Red_: long time angel friend to Kick, appears as a child but sometimes as adult, does not exist
    [0098](http://www.kick-girl.com/?comic=page-98)
    [0202](http://www.kick-girl.com/?comic=page-202)
    [0256](http://www.kick-girl.com/?comic=page-256)
*   _twin girls_: may be compromised by Teko, Kick cares about them
    [0372](http://www.kick-girl.com/?comic=page-372)
    [0373](http://www.kick-girl.com/?comic=page-373)
*   _Uriel_: Archangel, guardian of the pit, obliterated by Delilah:
    [255](http://www.kick-girl.com/?comic=page-255)
*   _Wiltz_: name familiar to Kick, Delilah, KillBoy
    [0383](http://www.kick-girl.com/?comic=page-383)
*   _Wynter_: Captain of the ship stolen by Kick and Delilah.
*   _Zero_: priest of Vesta becomes Stomp Boy thanks to Red
    [0218](http://www.kick-girl.com/?comic=page-218)
    [0324](http://www.kick-girl.com/?comic=page-324)

## The old Twelve Stars

*   _Cupid_
*   _Flora_
*   _Indra_
*   _Pulsar_: becomes second general of the Crusaders
    [0306](http://www.kick-girl.com/?comic=page-306)
    [0311](http://www.kick-girl.com/?comic=page-311)
*   _Shield_
*   _Solar_
*   _SuperVee_: becomes Vesta
*   _Sword_
*   _Zu_

## The new Twelve Stars (councillors)

*   All of them
    [0223](http://www.kick-girl.com/?comic=page-223)
*   _?_: _?_: guy, white skin, grey hair, brown eyes
    [0224](http://www.kick-girl.com/?comic=page-224)
*   _?_: _Mika_: girl, black skin, dark hair, violet eyes, killed by Teko
    [0223](http://www.kick-girl.com/?comic=page-223)
    [0226](http://www.kick-girl.com/?comic=page-226)
    [0266](http://www.kick-girl.com/?comic=page-266)
*   _Cupid_: _?_: girl, brown skin, brown hair, pale blue eyes
    [0223](http://www.kick-girl.com/?comic=page-223)
    [0375](http://www.kick-girl.com/?comic=page-375)
*   _Flora_: _Mari_: old lady, white skin, grey hair, pale blue eyes
    [0223](http://www.kick-girl.com/?comic=page-223)
    [0375](http://www.kick-girl.com/?comic=page-375)
*   _Indra_: _?_: girl, white skin, blonde hair, pale blue eyes
    [0375](http://www.kick-girl.com/?comic=page-375)
*   _JJ_: _Anara_: girl, light brown hair, white skin, orange eyes, Heaven's champion
    [220](http://www.kick-girl.com/?comic=page-220)
*   _Muse_: _?_ girl, brown skin, red hair, dark blue eyes
    [0375](http://www.kick-girl.com/?comic=page-375)
*   _Osyrus_: _Darren_: guy, white skin, black hair, grey eyes, killed by Teko
    [0223](http://www.kick-girl.com/?comic=page-223)
    [0264](http://www.kick-girl.com/?comic=page-264)
    [0267](http://www.kick-girl.com/?comic=page-267)
    [0364](http://www.kick-girl.com/?comic=page-364)
*   _Shield_: _?_: guy, brown skin, brown hair, brown eyes
    [0264](http://www.kick-girl.com/?comic=page-264)
    [0375](http://www.kick-girl.com/?comic=page-375)
*   _Solar_: _?_: man, black skin, no hair, violet eyes
    [0375](http://www.kick-girl.com/?comic=page-375)
*   _Sword_: _?_: girl, white skin, black hair, pale grey eyes
    [0264](http://www.kick-girl.com/?comic=page-264)
    [0375](http://www.kick-girl.com/?comic=page-375)
*   _Zu_: _Sarah_: girl, white skin, brown hair, grey eyes
    [0376](http://www.kick-girl.com/?comic=page-376)

## The old Niji Roku (Six of the Rainbox)

There seems to be overlap between some of them and the Twelve.

*   _JJ_: maybe?
    [0306](http://www.kick-girl.com/?comic=page-306)
    [0311](http://www.kick-girl.com/?comic=page-311)
    [0260](http://www.kick-girl.com/?comic=page-260)
*   _Sumire_: Girlfriend of JJ, assassin for hire, killed by Kick
    [0306](http://www.kick-girl.com/?comic=page-306)
    [0260](http://www.kick-girl.com/?comic=page-260)
*   _Ra_: becomes "War"
    [0338](http://www.kick-girl.com/?comic=page-338)
    [0347](http://www.kick-girl.com/?comic=page-347)

## The demons

*   _Adrian_: "Conquest", the saviour, the antichrist, the lord of light.
    [0164](http://www.kick-girl.com/?comic=page-164)
    [0182](http://www.kick-girl.com/?comic=page-182)
    [0305](http://www.kick-girl.com/?comic=page-305)
    [0370](http://www.kick-girl.com/?comic=page-370)
*   _Conquest_: one of the roles of Adrian, the saviour
*   _Death_: male, cowboy hat, skeleton hose
    [0182](http://www.kick-girl.com/?comic=page-182)
    [0186](http://www.kick-girl.com/?comic=page-186)
    [0370](http://www.kick-girl.com/?comic=page-370)
*   _Famine_: new role of Judas, white horse
    http://www.kick-girl.com/?p=1584
*   _Gog and Magog_:
    [0304](http://www.kick-girl.com/?comic=page-304)
*   _Judas_: follower of Delilah, becomes "Famine"
    [0186](http://www.kick-girl.com/?comic=page-186)
*   _Lilith_: now wife of Satan
    [0163](http://www.kick-girl.com/?comic=page-163)
    [0290](http://www.kick-girl.com/?comic=page-290)
    [0368](http://www.kick-girl.com/?comic=page-368)
    [0372](http://www.kick-girl.com/?comic=page-372)
*   _Pestilence_: girl, white hair, black horse with white mane
    [0182](http://www.kick-girl.com/?comic=page-182)
    [0186](http://www.kick-girl.com/?comic=page-186)
    [0370](http://www.kick-girl.com/?comic=page-370)
*   _Satan_: appears as a dragon
    [0161](http://www.kick-girl.com/?comic=page-161)
*   _Stalker of Babylon_: demon, probably related to Mystery Babylon
*   _Teko_: Little vampire demon working for Lilith
    [0364](http://www.kick-girl.com/?comic=page-364)
*   _War_: used to be Ra, red horse.
    [0182](http://www.kick-girl.com/?comic=page-182)
    [0186](http://www.kick-girl.com/?comic=page-186)
    [0338](http://www.kick-girl.com/?comic=page-338)
    [0347](http://www.kick-girl.com/?comic=page-347)
    [0371](http://www.kick-girl.com/?comic=page-371)

# Pages

*   [0001](http://www.kick-girl.com/?comic=page-1): Kick draws on ground herself and KB holding hands
*   [0026](http://www.kick-girl.com/?comic=page-26): Kick says her mother is Vesta
*   [0033](http://www.kick-girl.com/?comic=page-33): Zero persuades Kick by squirting her
*   [0039](http://www.kick-girl.com/?comic=page-39): Kick says that Vesta was not there for her when she needed saving
*   [0043](http://www.kick-girl.com/?comic=page-43): Kick says she used to be called Kick Girl
*   [0044 110415](http://www.kick-girl.com/?comic=page-110415): flashback: Kick as "The Monster" kills JJ on top of the demon pit
*   [0047 110425](http://www.kick-girl.com/?comic=page-110425): flashback: JJ makes Kick promise she won't free KB as he is the worst
*   [0054 110520](http://www.kick-girl.com/?comic=page-110520): Anara tells Kick she hears music
*   [0095 120514](http://www.kick-girl.com/?comic=page-120514): Kick, Zero, Judas find statues of JJ with sword
*   [0098 120611](http://www.kick-girl.com/?comic=page-120611): flashback: Kick in Fallout Temple with Red, Super Vee is her mother
*   [0116 121112](http://www.kick-girl.com/?comic=page-121112): Judas call Kick "Six" to stop her demon form
*   [0125 130107](http://www.kick-girl.com/?comic=page-130107): Kick says only one other than Zero could control the monster
*   [0129 130204](http://www.kick-girl.com/?comic=page-130204): Kick says Super Vee was never there for her or her sisters
*   [0130 130211](http://www.kick-girl.com/?comic=page-130211): Judas asks about Kick's father
*   [0133 130304](http://www.kick-girl.com/?comic=page-130304): flashback: Kick saved from the pit by Red, and cannot save KillBoy 
*   [0144 130520](http://www.kick-girl.com/?comic=page-130520): Kick and Zero find the atomic bomb craters
*   [0147 130610](http://www.kick-girl.com/?comic=page-130610): Kick kisses Zero and likes it
*   [0154 130729](http://www.kick-girl.com/?comic=page-130729): Kick on the seal notices Judas has the mark
*   [0156 130812](http://www.kick-girl.com/?comic=page-130812): Judas kisses Kick and kills her, and the demon's blood opens the seal
*   [0163 130923](http://www.kick-girl.com/?comic=page-130923): The Pit open and Lilith comes out and Adrian appears
*   [0166 131021](http://www.kick-girl.com/?comic=page-131021): flashback: Kick in game uses name "Lady Feylyn" playing with KB.
*   [0170 131118](http://www.kick-girl.com/?comic=page-131118): Anara and Raz fight watched by Arcadia
*   [0178 140120](http://www.kick-girl.com/?comic=page-140120): Anara says that the dragon is Satan
*   [0180 140203](http://www.kick-girl.com/?comic=page-140203): Lilith appears and tells Teko to sabotage Anara
*   [0181 140210](http://www.kick-girl.com/?comic=page-140210): Judas says that Adrian is actually the Lord Of The Light
*   [0182 140217](http://www.kick-girl.com/?comic=page-140217): Adrian says he is Death in the Apocalypse
*   [0183 150224](http://www.kick-girl.com/?comic=page-150224): Kick says to spare Delilah, and Adrian just marks her
*   [0186 140317](http://www.kick-girl.com/?comic=page-140317): Adrian with the four horsemen of the Apocalypse.
*   [0190 140414](http://www.kick-girl.com/?comic=page-140414): KillBoy wants to kill Kick and himself
*   [0193 140505](http://www.kick-girl.com/?comic=page-140505): Kick says KillBoy kills those who care for her
*   [0202 140707](http://www.kick-girl.com/?comic=page-140707): "Red" asks Kick to join, she refuses and he becomes adult
*   [0203 140714](http://www.kick-girl.com/?comic=page-140714): "Red" explains the marks
*   [0204 140721](http://www.kick-girl.com/?comic=page-140721): "Red" tells Kick to find Heaven's new champion
*   [0205 140728](http://www.kick-girl.com/?comic=page-140728): "Red" tells Kick the world will end and he is her friend
*   [0211 140908](http://www.kick-girl.com/?comic=page-140908): Delilah saves Kick from KB
*   [0212 140915](http://www.kick-girl.com/?comic=page-140915): flashback: Six tells JJ that she could be Sidekick Girl
*   [0215 141006](http://www.kick-girl.com/?comic=page-141006): KB mentions her parents secret and she says the 12 assholes
*   [0217 141020](http://www.kick-girl.com/?comic=page-141020): "Red" as adult gives mark, holy water, costume to Zero
*   [0222 140924](http://www.kick-girl.com/?comic=page-140924): Annoying black demon in cathedral following Anara
*   [0244 150427](http://www.kick-girl.com/?comic=page-150427): K-Scum mask among relics of 12 stars
*   [0245 150504](http://www.kick-girl.com/?comic=page-150504): Kick shows relics of the 12 superheroes including Solar Core
*   [0247 150518](http://www.kick-girl.com/?comic=page-150518): Kick scolds Delilah for looking more like a slut
*   [0248 150525](http://www.kick-girl.com/?comic=page-150525): Kick celebrates that Delilah is jealous, they used to be close
*   [0252 150622](http://www.kick-girl.com/?comic=page-150622): Kick says that Adrian is the antichrist
*   [0256 150720](http://www.kick-girl.com/?comic=page-150720): Archangel Uriel cannot harm them because mark of God.
*   [0256 150720](http://www.kick-girl.com/?comic=page-150720): There is no angel called "Red"
*   [0258 150803](http://www.kick-girl.com/?comic=page-150803): Delilah gets Cupid's bow
*   [0260](http://www.kick-girl.com/?comic=page-260): flashback: before the apocalypse with Sumire, JJ, K-Scum
*   [0259 150810](http://www.kick-girl.com/?comic=page-150810): K-Scum mentions Kick's sister, MS. J, made a film about her
*   [0264](http://www.kick-girl.com/?comic=page-264): One of the 12 names the Osyrus, Shield, Sword. Mika is dead
*   [0266](http://www.kick-girl.com/?comic=page-266): Anara finds first Mika, first killed by Teko
*   [0270](http://www.kick-girl.com/?comic=page-270): Cupid's bow is one of several. Also it obliterated the archangel Uriel
*   [0276](http://www.kick-girl.com/?comic=page-276): Kick says don't kill KB becase they share the same soul
*   [0289](http://www.kick-girl.com/?comic=page-289): Delilah talks with witch about black mark
*   [0290 160314](http://www.kick-girl.com/?comic=page-160314): The witch is Lilith
*   [0300](http://www.kick-girl.com/?comic=page-300): Kick fights Wynter who sees her eyes are red
*   [0305](http://www.kick-girl.com/?comic=page-305): Adrian waits for "Red" who loves him; Adrian switches between white and black speech
*   [0306](http://www.kick-girl.com/?comic=page-306): flashback: just after the sealing, JJ, Sumire and Pulsar at the Pit
*   [0309](http://www.kick-girl.com/?comic=page-309): flashback: "Red" appears when JJ wants to attack Kick at the Pit
*   [0311](http://www.kick-girl.com/?comic=page-311): "Red" says all mortal people who attempt to kill her will die
*   [0321](http://www.kick-girl.com/?comic=page-321): idiot cultists reveal Delilah and Kick to the Crusaders
*   [0324 161107](http://www.kick-girl.com/?comic=page-161107): Zero becomes Stomp Boy
*   [0325 161114](http://www.kick-girl.com/?comic=page-161114): Kick figures out Judas was with Delilah
*   [0330 161219](http://www.kick-girl.com/?comic=page-161219): Stomp Boy frees Delilah and Kick
*   [0337 170206](http://www.kick-girl.com/?comic=page-170206): War calls Kick "Harlot" and KillBoy "Beast"
*   [0338](http://www.kick-girl.com/?comic=page-338): "War" appears and mentions Sumire and the Niji-Roku
*   [0343](http://www.kick-girl.com/?comic=page-343): Kick as the demon kills War
*   [0369](http://www.kick-girl.com/?comic=page-369): Teko says that Lilith would spare Six and Adrian too
*   [0369](http://www.kick-girl.com/?comic=page-369): Lilith says the female one as "Pestilence" and the one with the hat as "Death"
*   [0370](http://www.kick-girl.com/?comic=page-370): Lilith refers to Adrian as "Conquest".
*   [0371](http://www.kick-girl.com/?comic=page-371): Lilith says her husband Lucifer wold not approve but is distracted.
*   [0372](http://www.kick-girl.com/?comic=page-372): Teko says to Kick she met Lilith when she was Six
*   [0375](http://www.kick-girl.com/?comic=page-375): The councillors name themselves
*   [0378](http://www.kick-girl.com/?comic=page-378): Kick and Anara have a good moment and they hear music when around
*   [0387](http://www.kick-girl.com/?comic=page-387): Delilah tries to kill Anara but unlike Uriel she takes no damage
*   [0395](http://www.kick-girl.com/?comic=page-395): Adrian talking with "Red" mentions his father and Vesta
*   [0398 180409](http://www.kick-girl.com/?comic=page-180409): "Red" becomes huge and judges the crusaders

# Notes from author

https://disqus.com/by/kickgirl/

# EMACS settings

    ;;; Local Variables:		***
    ;;; mode: markdown			***
    ;;; mode: outline-minor		***
    ;;; mode: whitespace-newline	***
    ;;; org-descriptive-links: nil	***
    ;;; fill-column: 72			***
    ;;; truncate-lines: t		***
    ;;; indent-tabs-mode: t		***
    ;;; tab-width: 8			***
    ;;; case-fold-search: t		***
    ;;; End:				***
    vim:set noet nowrap ts=8 tw=78 sw=2:
